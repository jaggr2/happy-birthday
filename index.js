var figlet = require('figlet'),
    colors = require('colors');


figlet("Happy B'day, Marco!", {
    font: "Larry 3D",
    horizontalLayout: "controlled smushing"
}, function (err, art) {
    console.log(colors.bgBlack.white(art));
});